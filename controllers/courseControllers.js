const Course = require("./../models/Courses");


module.exports.getAllActiveCourses = () => {

	return Course.find({isActive: true}).then(result => {
		return result
	})

}

//get single course
module.exports.getSingleCourse = (params) => {
	//console.log(params)
	return Course.findById(params.courseId).then(course=>{
		return course
	})
}
//----------------------------------------------------


//get all
module.exports.getAllCourses = ()=> {
	return Course.find({}).then(result => {
		return result
	})
}


//add course 
module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((result, error)=>{
		if (error) {
			return error
		} else {
			return true //if course is added
		}
	});
}

//edit course
module.exports.editCourse = (params, reqBody)=> {
	
	let updatedCourse = ({
		name: reqBody.name,
		price: reqBody.price,
		description: reqBody.description
	})
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true}).then((result, error) => {
		if (error) {
			return error
		} else {
			return result
		}
	})
}

//archive course
module.exports.archiveCourse = (params)=> {

	let updatedActiveCourse = {
		isActive : false
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then((result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//unarchive course
module.exports.unarchiveCourse = (params)=> {

	let updatedActiveCourse = {
		isActive : true
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then((result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//delete course
module.exports.deleteCourse = (params)=> {

	return Course.findByIdAndDelete(params).then((result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

