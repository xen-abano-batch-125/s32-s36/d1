//we will use this for authentication
	//login
	//retrieve user details

//JSON Web Token
	//3 methods to be used
		//sign(data, secret, {options}) - create a token
		//verify(token, secret, callback()) - checks if the token is present
		//decode(token, {options}).payload - interpret/decode the created token.

const jwt = require('jsonwebtoken');
const secret = "CourseBookingApi"; // can be any string


//create token
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}


// verify token
module.exports.verify = (req, res, next)=> {
	let token = req.headers.authorization

	//console.log(token);

	//if token is present or not undefined
	if (typeof token !== "undefined") {
		
		//to remove the "bearer" word in the token
		token = token.slice(7, token.length);
		//console.log(token)
		return jwt.verify(token, secret, (error,data)=>{
			if (error) {
				return res.send({auth:"authentication failed."})
			} else {
				next();
			}
		})
	}
}

//decode token
module.exports.decode = (token)=> {

	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error,data)=>{
			if (error) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload //can also be used without verify
			}
		})
	}
}
//decode(token, {options}).payload - interpret/decode the created token.

