const express = require('express');
const router = express.Router();
const auth = require('./../auth');

//controllers
const userController = require("./../controllers/userControllers")

//route that checks if the email exists
router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExists(req.body).then(result=>res.send(result));
});

//user registration 
router.post("/register", (req,res)=> {
	userController.registerUser(req.body).then(result=>res.send(result));
});

//login
router.post("/login", (req,res)=>{
	userController.login(req.body).then(result=>res.send(result));
});

//retrieve details
router.get("/details", auth.verify, (req,res)=> {

	const userData = auth.decode(req.headers.authorization)
	
	userController.getProfile(userData.id).then(result=>res.send(result));
})

//enrollments
router.post("/enroll", auth.verify, (req,res)=>{

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}
	//console.log(data)
	userController.enroll(data).then(result=>res.send(result));
});

//update user
router.put('/:userId/edit', auth.verify,(req,res)=>{
	//console.log(req.params.userId)
	userController.editProfile(req.params.userId, req.body).then(result => res.send(result));
})

module.exports = router;